import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:virtual_keyboard/virtual_keyboard.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
     
        primarySwatch: Colors.blue,
       
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final controller = FlutterWebviewPlugin();

   String text = '';

  // True if shift enabled.
  bool shiftEnabled = false;

  // is true will show the numeric keyboard.
  bool isNumericMode = true;

  Future <bool> _exitApp() async {
  
  if (await controller.canGoBack()) {
    controller.goBack();
    print('controller.goBack');
   // return true;
  } 
    else{
      print('object');
      exit(0);
      return false; 
    }
  //  return true;
  
  
}
 
  @override
  Widget build(BuildContext context) {

    return Scaffold(
    
      body: WillPopScope(
        onWillPop: _exitApp,
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 4,
                child: WebviewScaffold(
           url: 'https://s3.amazonaws.com/stemfuse_games/gameit/asteroid_destroyer/section3/index.html',
           withJavascript: true,
           ),
                  ),
                  Expanded(
                    flex: 6,
                    child:  Center(
        child: Column(
          children: <Widget>[
           
          //  FlatButton(child: Text('Enter'),
          //  onPressed: (){},
          //  )
           
Text(
              text,
              style: Theme.of(context).textTheme.display1,
            ),
            SwitchListTile(
              title: Text(
                'Keyboard Type = ' +
                    (isNumericMode
                        ? 'VirtualKeyboardType.Numeric'
                        : 'VirtualKeyboardType.Alphanumeric'),
              ),
              value: isNumericMode,
              onChanged: (val) {
                setState(() {
                  isNumericMode = val;
                });
              },
            ),
            Expanded(
              child: Container(),
            ),
            Container(
              color: Colors.deepPurple,
              child: VirtualKeyboard(
                  height: 300,
                  textColor: Colors.white,
                  type: isNumericMode
                      ? VirtualKeyboardType.Numeric
                      : VirtualKeyboardType.Alphanumeric,
                  onKeyPress: _onKeyPress),
            )

          ],
        ),
                    )
                    )
                ],
              ),
      ),
       // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  _onKeyPress(VirtualKeyboardKey key) {
    if (key.keyType == VirtualKeyboardKeyType.String) {
      text = text + (shiftEnabled ? key.capsText : key.text);
    } else if (key.keyType == VirtualKeyboardKeyType.Action) {
      switch (key.action) {
        case VirtualKeyboardKeyAction.Backspace:
          if (text.length == 0) return;
          text = text.substring(0, text.length - 1);
          break;
        case VirtualKeyboardKeyAction.Return:
          text = text + '\n';
          break;
        case VirtualKeyboardKeyAction.Space:
          text = text + key.text;
          break;
        case VirtualKeyboardKeyAction.Shift:
          shiftEnabled = !shiftEnabled;
          break;
        default:
      }
    }
    // Update the screen
    setState(() {});
  }
}
